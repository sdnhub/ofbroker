package org.sdnhub.odl.ofbroker;

/**
  * @author Madhu Venugopal (mavenugo@gmail.com)
  */

import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

/**
 * This interface defines low level routines to read/write messages on an open
 * socket channel. If secure communication is desired, these methods also perform
 * encryption and decryption of the network data.
 */
public interface IMessageRead {
        /**
         * Reads the incoming network data from the socket and retrieves the OF
         * messages. For secure communication, the data will be decrypted first.
         *
         * @return list of OF messages
         * @throws Exception
         */
    public ByteBuffer readMessages() throws Exception;

        /**
         * Proper clean up when the switch connection is closed
         *
         * @return
         * @throws Exception
         */
    public void stop() throws Exception;

    public Selector getSelector();

    public SocketChannel getSocket();
}
