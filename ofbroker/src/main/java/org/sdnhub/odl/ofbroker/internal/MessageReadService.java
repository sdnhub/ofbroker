package org.sdnhub.odl.ofbroker.internal;

/**
 * @author Madhu Venugopal (mavenugo@gmail.com)
 */

import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

import org.sdnhub.odl.ofbroker.IMessageRead;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements methods to read/write messages over an established
 * socket channel. The data exchange is in clear text format.
 */
public class MessageReadService implements IMessageRead {
    private static final Logger logger = LoggerFactory
            .getLogger(MessageReadService.class);
    private static final int bufferSize = 1024 * 1024;

    private Selector selector;
    private SelectionKey clientSelectionKey;
    private SocketChannel socket;
    private ByteBuffer inBuffer;

    public MessageReadService(SocketChannel socket, Selector selector)
            throws ClosedChannelException {
        this.socket = socket;
        this.selector = selector;
        this.inBuffer = ByteBuffer.allocateDirect(bufferSize);
        this.clientSelectionKey = this.socket.register(this.selector,
                SelectionKey.OP_READ);
    }

    public Selector getSelector() {
        return selector;
    }

    public SocketChannel getSocket() {
        return socket;
    }

    /**
     * Reads the incoming network data from the socket and retrieves the OF
     * messages.
     *
     * @return Raw ByteBuffer to parse
     * @throws Exception
     */
    @Override
    public ByteBuffer readMessages() throws Exception {
        if (!socket.isOpen()) {
            return null;
        }

        int bytesRead = -1;
        bytesRead = socket.read(inBuffer);
        if (bytesRead < 0) {
            return null;
        }
        inBuffer.flip();
        return inBuffer;
    }

    @Override
    public void stop() {
        inBuffer = null;
    }
}
