package org.sdnhub.odl.ofbroker;

/**
 * @author Madhu Venugopal (mavenugo@gmail.com)
 */

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public interface IOFPlugin {
    boolean isSupported(ByteBuffer buffer);
    boolean takeOver(SocketChannel sc);
}
