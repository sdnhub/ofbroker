package org.sdnhub.odl.ofbroker.internal;

/**
 * @author Madhu Venugopal (mavenugo@gmail.com)
 */

import org.apache.felix.dm.Component;
import org.opendaylight.controller.sal.core.ComponentActivatorAbstractBase;
import org.sdnhub.odl.ofbroker.IBroker;
import org.sdnhub.odl.ofbroker.IOFPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Activator extends ComponentActivatorAbstractBase {
    protected static final Logger logger = LoggerFactory
            .getLogger(Activator.class);

    /**
     * Function that is used to communicate to dependency manager the list of
     * known implementations for services that are container independent.
     *
     *
     * @return An array containing all the CLASS objects that will be
     *         instantiated in order to get an fully working implementation
     *         Object
     */
    @Override
    public Object[] getGlobalImplementations() {
        Object[] res = { Broker.class };
        return res;
    }

    /**
     * Function that is called when configuration of the dependencies is
     * required.
     *
     * @param c
     *            dependency manager Component object, used for configuring the
     *            dependencies exported and imported
     * @param imp
     *            Implementation class that is being configured, needed as long
     *            as the same routine can configure multiple implementations
     */
    @Override
    public void configureGlobalInstance(Component c, Object imp) {
        if (imp.equals(Broker.class)) {
            c.setInterface(new String[] { IBroker.class.getName()},
                                          null);

            c.add(createServiceDependency()
                    .setService(IOFPlugin.class, "(name=Controller)")
                    .setCallbacks("setOFPlugin", "unsetOFPlugin")
                    .setRequired(false));
        }
    }
}
