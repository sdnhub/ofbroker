package org.sdnhub.odl.ofbroker.internal;

/**
 * @author Madhu Venugopal (mavenugo@gmail.com)
 */


import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.sdnhub.odl.ofbroker.IBroker;
import org.sdnhub.odl.ofbroker.IMessageRead;
import org.sdnhub.odl.ofbroker.IOFPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Broker implements IBroker {
    private static final Logger logger = LoggerFactory
            .getLogger(Broker.class);
    private BrokerIO brokerIO;
    private Set<IOFPlugin> ofPlugins = new HashSet<IOFPlugin>();
    private AtomicInteger switchInstanceNumber = new AtomicInteger(0);

    public void init() {
        logger.debug("Initializing");
        ofPlugins = new HashSet<IOFPlugin>();
        this.switchInstanceNumber = new AtomicInteger(0);

        // Disabling the legacy OpenFlow protocol plugin
        BundleContext bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();

        for(Bundle bundle : bundleContext.getBundles()) 
            if (bundle.getSymbolicName().contains("org.opendaylight.controller.protocol_plugins.openflow") ||
                bundle.getSymbolicName().contains("org.opendaylight.controller.thirdparty.org.openflow.openflowj")) {
                try {
                    bundle.uninstall();
                }
                catch (BundleException e) {
                    logger.error("Exception in Bundle uninstall "+bundle.getSymbolicName(), e); 
                }   
            }   
    }

    public void setOFPlugin(IOFPlugin ofp) {
        ofPlugins.add(ofp);
    }

    public void unsetOFPlugin(IOFPlugin ofp) {
        ofPlugins.remove(ofp);
    }

    /**
     * Function called by dependency manager after "init ()" is called and after
     * the services provided by the class are registered in the service registry
     *
     */
    public void start() {
        logger.debug("Starting!");
        // spawn a thread to start to listen on the open flow port
        brokerIO = new BrokerIO(this);
        try {
            brokerIO.start();
        } catch (IOException ex) {
            logger.error("Caught exception while starting:", ex);
        }
    }

    /**
     * Function called by the dependency manager before the services exported by
     * the component are unregistered, this will be followed by a "destroy ()"
     * calls
     *
     */
    public void stop() {
        try {
            brokerIO.shutDown();
        } catch (IOException ex) {
            logger.error("Caught exception while stopping:", ex);
        }
    }

    /**
     * Function called by the dependency manager when at least one dependency
     * become unsatisfied or when the component is shutting down because for
     * example bundle is being stopped.
     *
     */
    public void destroy() {
    }

    private IMessageRead getMessageReadWriteService(SocketChannel socket, Selector selector) throws Exception {
        socket.configureBlocking(false);
        socket.socket().setTcpNoDelay(true);

        String str = System.getProperty("secureChannelEnabled");
        return ((str != null) && (str.trim().equalsIgnoreCase("true"))) ? new SecureMessageReadService(socket,
                selector) : new MessageReadService(socket, selector);
    }

    public boolean handleMessages(IMessageRead msgReadWriteService) throws Exception {
        ByteBuffer buffer;
        if (msgReadWriteService != null) {
            buffer = msgReadWriteService.readMessages();
            if (buffer != null) {
                ByteBuffer dup = null;
                for (IOFPlugin ofp : ofPlugins) {
                    dup = buffer.duplicate();
                    if (ofp.isSupported(dup)) {
                        ofp.takeOver(msgReadWriteService.getSocket());
                        return true;
                    }
                }
                if (dup != null && dup.hasRemaining()) {
                    buffer = dup.duplicate();
                    buffer.compact();
                } else {
                    buffer.clear();
                }
            }
        }
        return false;
    }

    private void startHandlerThread(SocketChannel sc, String instanceName) throws Exception {
        final SocketChannel sockChannel = sc;
        final Selector selector = SelectorProvider.provider().openSelector();
        final IMessageRead msgReadWriteService = getMessageReadWriteService(sc, selector);
        Thread switchHandlerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        // wait for an incoming connection
                        selector.select(0);
                        Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
                        while (selectedKeys.hasNext()) {
                            SelectionKey skey = selectedKeys.next();
                            selectedKeys.remove();
                            if (skey.isValid() && skey.isReadable()) {
                                if (handleMessages(msgReadWriteService)) {
                                    return;
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        try { 
                            logger.info("Switch {} is in bad state. Disconnecting.",
                                sockChannel.socket().getRemoteSocketAddress()
                                .toString().split("/")[1]);
                            sockChannel.close();
                        } catch (Exception e2) { }
                        return;
                    }
                }
            }
        }, instanceName);
        switchHandlerThread.start();
    }

    public void handleNewConnection(Selector selector,
            SelectionKey serverSelectionKey) {
        ServerSocketChannel ssc = (ServerSocketChannel) serverSelectionKey
                .channel();
        SocketChannel sc = null;
        try {
            sc = ssc.accept();
            // create new switch
            int i = this.switchInstanceNumber.addAndGet(1);
            String instanceName = "SwitchHandler-" + i;
            this.startHandlerThread(sc, instanceName);
            if (sc.isConnected()) {
                logger.info("Switch:{} is connected to Broker",
                        sc.socket().getRemoteSocketAddress()
                        .toString().split("/")[1]);
            } 

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }
}
